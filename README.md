# Roman Numerals Kata


The Romans were a clever bunch. They conquered most of Europe and ruled it for hundreds of years. They invented concrete and straight roads and even bikinis [1]. One thing they never discovered though was the number zero. This made writing and dating extensive histories of their exploits slightly more challenging, but the system of numbers they came up with is still in use today. For example the BBC uses Roman numerals to date their programmes.

The Romans wrote numbers using letters : I, V, X, L, C, D, M. (notice these letters have lots of straight lines and are hence easy to hack into stone tablets)

## Part 1

The Kata says you should write a function to convert from normal numbers to Roman Numerals: eg
```
     1 --> I
     10 --> X
     7 --> VII
```
etc.

There is no need to be able to convert numbers larger than about 3000. (The Romans themselves didn’t tend to go any higher)


## General requirements

* Use whatever language and frameworks you want. Use something that you know well.
* Provide a README with instructions on how to compile and run the application.

**IMPORTANT**:  Implement the requirements focusing on writing the best code you can produce.

**CODE SUBMISSION**: Add the code to your own account and send us the link.
